#!/usr/bin/env python3

import numpy as np

v = 12.

t = np.arange(3.,120.,.3)
t += np.random.normal(scale=2.,size=t.shape)
t = np.sort(t)

x = t*v
x += np.random.normal(scale=50.,size=x.shape)

out=np.stack((t,x),axis=-1)
np.savetxt('hare.txt',out)

xw = -.15*(t-120)**2+1050
tw = t[xw >= 0]
xw = -.15*(tw-120)**2+1050

xw = np.tile(xw,(5,1))
xw += np.random.normal(scale=50.,size=xw.shape)

print(xw.shape)
t2 = np.reshape(tw,(1,tw.shape[0]))
print(t2.shape)
out = np.concatenate((t2,xw))

#out=np.stack((t,xw),axis=-1)
np.savetxt('wolves.txt',out.T)

age = np.full(tw.shape,2000) + np.random.normal(scale=10.,size=tw.shape)
np.savetxt('deaths.txt',age.astype(int),fmt="%d")
